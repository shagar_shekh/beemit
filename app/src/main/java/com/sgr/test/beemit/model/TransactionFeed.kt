package com.sgr.test.beemit.model

import java.util.*

/**
 * Created by Shekh Shagar on 07/06/2018
 *
 * Used for adapter item {@link TransactionRecyclerAdapter}
 */
abstract class TransactionFeed(val type: Int) {
    class TYPE {
        companion object {
            const val VIEW_TYPE_ACCOUNT_INFO = 0
            const val VIEW_TYPE_DATE = 1
            const val VIEW_TYPE_TRANSACTION = 2
        }
    }
}

data class AccountInfoItem(
        var accountInfo: AccountInfo
) : TransactionFeed(TYPE.VIEW_TYPE_ACCOUNT_INFO)

data class DateItem(
        var calendar: Calendar
) : TransactionFeed(TYPE.VIEW_TYPE_DATE)

data class TransactionItem(
        var transaction: Transaction,
        var atmInfo: AtmInfo?
) : TransactionFeed(TYPE.VIEW_TYPE_TRANSACTION)
