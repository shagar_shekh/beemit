package com.sgr.test.beemit.network

import android.annotation.SuppressLint
import android.content.Context
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley

/**
 * Created by Shekh Shagar on 06/06/2018
 *
 * Maintains RequestQueue for volley request
 */
class RequestQueueHelper(context: Context) {

    private var mContext = context
    private var mRequestQueue: RequestQueue = Volley.newRequestQueue(mContext.applicationContext)

    companion object {
        @JvmField
        val LOG_TAG: String = RequestQueueHelper::class.java.simpleName

        @SuppressLint("StaticFieldLeak")
        var mInstance: RequestQueueHelper? = null

        @Synchronized
        fun getInstance(context: Context): RequestQueueHelper? {
            if (mInstance == null) {
                mInstance = RequestQueueHelper(context)
            }
            return mInstance
        }
    }

    fun getRequestQueue(): RequestQueue {
        return mRequestQueue
    }

    fun <T> addToRequestQueue(request: Request<T>) {
        mRequestQueue.add(request)
    }

    fun cancelAllRequests() {
        mRequestQueue.cancelAll(mContext.applicationContext)
    }
}