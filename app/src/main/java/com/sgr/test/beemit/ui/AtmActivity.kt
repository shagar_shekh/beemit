package com.sgr.test.beemit.ui

import android.os.Bundle
import android.view.View
import com.sgr.test.beemit.R
import com.sgr.test.beemit.model.AtmInfo
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.toolbar.view.*

/**
 * Created by Shekh Shagar on 06/06/2018
 */
class AtmActivity : BaseActivity() {

    companion object {
        @JvmField
        val LOG_TAG: String = AtmActivity::class.java.simpleName
        const val INTENT_EXTRA_ATM_INFO = "INTENT_EXTRA_ATM_INFO"
    }

    private var mAtmInfo: AtmInfo? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.atm_activity)
        initActionBar()

        mAtmInfo = intent?.extras?.getParcelable(INTENT_EXTRA_ATM_INFO) as AtmInfo?

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as MapFragment
        mapFragment.setAtmInfo(mAtmInfo)
        mapFragment.getMapAsync(mapFragment)
    }

    private fun initActionBar() {
        app_toolbar.toolbar_title.setText(R.string.find_us)
        app_toolbar.back_button.visibility = View.VISIBLE
        app_toolbar.back_button.setOnClickListener { onBackPressed() }
        setSupportActionBar(app_toolbar)
    }
}
