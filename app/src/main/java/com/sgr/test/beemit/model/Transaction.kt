package com.sgr.test.beemit.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

/**
 * Created by Shekh Shagar on 06/06/2018
 */
@Parcelize
data class Transaction(var id: String?,
                       var date: Calendar?,
                       var effectiveDate: String?,
                       var description: String?,
                       var atmId: String?,
                       var amount: Double?,
                       var isPending: Boolean?) : Parcelable
