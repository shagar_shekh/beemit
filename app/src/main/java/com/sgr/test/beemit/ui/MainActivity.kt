package com.sgr.test.beemit.ui

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.sgr.test.beemit.R
import com.sgr.test.beemit.api.AccountTransactionApi
import com.sgr.test.beemit.model.AtmInfo
import com.sgr.test.beemit.model.TransactionFeed
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.toolbar.view.*

/**
 * Created by Shekh Shagar on 06/06/2018
 *
 * Landing Activity shows Account Info and Transactions
 */
class MainActivity : BaseActivity() {

    companion object {
        @JvmField
        val LOG_TAG: String = MainActivity::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initActionBar()
        main_swipe_refresh_layout.setOnRefreshListener { makeRequest() }
        makeRequest()
    }

    private fun refreshList(list: ArrayList<TransactionFeed>) {
        val adapter = TransactionRecyclerAdapter(applicationContext, list, { atmInfo ->
            goToAtmActivity(atmInfo)
        })
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        main_recycler_view.itemAnimator = DefaultItemAnimator()
        main_recycler_view.layoutManager = layoutManager
        main_recycler_view.adapter = adapter
        adapter.notifyDataSetChanged()
        showHideEmptyListMessage(true)
    }

    private fun goToAtmActivity(atmInfo: AtmInfo) {
        val intent = Intent(this, AtmActivity::class.java)
        intent.putExtra(AtmActivity.INTENT_EXTRA_ATM_INFO, atmInfo)
        startActivity(intent)
    }

    private fun makeRequest() {
        main_swipe_refresh_layout?.isRefreshing = true
        main_empty_list_text_view?.setText(R.string.feed_empty_list_message)

        AccountTransactionApi.getInstance(this@MainActivity)?.getAccountTransactionDetails(
                {
                    main_swipe_refresh_layout?.isRefreshing = false
                    refreshList(it)
                    showHideEmptyListMessage(false)
                },
                {
                    Log.i(LOG_TAG, "responseError: " + it.toString())
                    main_empty_list_text_view?.setText(R.string.feed_empty_list_error_message)
                    val snackbarMessage = if (isNetworkConnected)
                        getString(R.string.snackbar_feed_load_error)
                    else
                        getString(R.string.snackbar_network_error_message)

                    Snackbar.make(main_recycler_view, snackbarMessage, Snackbar.LENGTH_INDEFINITE)
                            .setAction(R.string.refresh, { makeRequest() })
                            .show()

                    showHideEmptyListMessage(true)
                    main_swipe_refresh_layout?.isRefreshing = false
                })
    }

    private fun showHideEmptyListMessage(showMessage: Boolean) {
        main_empty_list_text_view.visibility = if (showMessage) View.VISIBLE else View.GONE
        main_recycler_view.visibility = if (showMessage) View.GONE else View.VISIBLE
    }

    private fun initActionBar() {
        app_toolbar.toolbar_title.setText(R.string.account_details)
        app_toolbar.back_button.visibility = View.GONE
        setSupportActionBar(app_toolbar)
    }
}
