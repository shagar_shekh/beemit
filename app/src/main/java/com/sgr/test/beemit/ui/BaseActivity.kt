package com.sgr.test.beemit.ui

import android.content.Context
import android.net.ConnectivityManager
import android.support.v7.app.AppCompatActivity
import com.sgr.test.beemit.network.RequestQueueHelper

/**
 * Created by Shekh Shagar on 06/06/2018
 *
 * Parent Activity used globally in the app, contains reusable methods
 */
open class BaseActivity : AppCompatActivity() {

    protected val isNetworkConnected: Boolean
        get() {
            val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = connectivityManager.activeNetworkInfo
            return networkInfo != null && networkInfo.isConnectedOrConnecting
        }

    override fun onStop() {
        super.onStop()
        RequestQueueHelper.getInstance(this)?.cancelAllRequests()
    }
}
