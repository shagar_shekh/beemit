package com.sgr.test.beemit.network

import android.content.Context
import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest

/**
 * Created by Shekh Shagar on 06/06/2018
 *
 * Used to make volley request
 */
class RequestsProcessor(context: Context, requestResponseListener: RequestResponseListener) {

    companion object {
        @JvmField
        val LOG_TAG: String = RequestsProcessor::class.java.simpleName
    }

    private var mContext = context
    private var mRequestResponseListener = requestResponseListener

    /**
     * Handles get request
     *
     * @param url String
     */
    fun get(url: String) {
        try {
            val objectRequest = JsonObjectRequest(
                    Request.Method.GET, url, null,
                    Response.Listener { response ->
                        mRequestResponseListener.responseOk(response.toString())
                    },
                    Response.ErrorListener { error ->
                        mRequestResponseListener.responseError(error)
                    }
            )
            objectRequest.tag = mContext.applicationContext
            RequestQueueHelper.getInstance(mContext)?.addToRequestQueue(objectRequest)
        } catch (e: Exception) {
            Log.e(LOG_TAG, "Exception in makeRequest: " + e.toString())
        }
    }

    interface RequestResponseListener {
        fun responseOk(responseStr: String)

        fun responseError(error: VolleyError)
    }
}
