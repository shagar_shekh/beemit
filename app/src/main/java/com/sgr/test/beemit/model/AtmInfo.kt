package com.sgr.test.beemit.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by Shekh Shagar on 06/06/2018
 */
@Parcelize
data class AtmInfo(var id: String?,
                   var name: String?,
                   var address: String?,
                   var location: Location?) : Parcelable
