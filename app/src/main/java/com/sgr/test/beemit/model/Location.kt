package com.sgr.test.beemit.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by Shekh Shagar on 06/06/2018
 */
@Parcelize
data class Location(var lat: Double?,
                    var lng: Double?) : Parcelable
