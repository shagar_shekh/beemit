package com.sgr.test.beemit.api

import android.annotation.SuppressLint
import android.app.Activity
import com.android.volley.VolleyError
import com.google.gson.Gson
import com.sgr.test.beemit.model.*
import com.sgr.test.beemit.network.RequestsProcessor
import com.sgr.test.beemit.util.DateUtils
import com.sgr.test.beemit.util.NetworkConstants
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by Shekh Shagar on 06/06/2018
 *
 * Retrieves data from web and prepare {@link TransactionFeed} list for the adapter used by {@link MainActivity}
 */
class AccountTransactionApi private constructor(activity: Activity) {

    private var mActivity: Activity = activity

    companion object {
        @SuppressLint("StaticFieldLeak")
        private var instance: AccountTransactionApi? = null

        fun getInstance(activity: Activity): AccountTransactionApi? {
            if (instance == null) {
                instance = AccountTransactionApi(activity)
            }
            return instance
        }
    }

    /**
     * get the response form server calls prepareTransactionFeed to parse the data and invoke TransactionFeed list
     *
     * @param successListener listener for successful data retrieval and parse
     * @param errorListener listener for error
     */
    fun getAccountTransactionDetails(successListener: (transactionFeedList: ArrayList<TransactionFeed>) -> Unit, errorListener: (error: VolleyError) -> Unit) {
        RequestsProcessor(mActivity.applicationContext!!, object : RequestsProcessor.RequestResponseListener {
            override fun responseOk(responseStr: String) {
                val transaction = Gson().fromJson(responseStr, AccountTransactionResponse::class.java)
                successListener.invoke(prepareTransactionFeed(transaction))
            }

            override fun responseError(error: VolleyError) {
                errorListener.invoke(error)
            }
        }).get(NetworkConstants.URL_ACCOUNT_DETAILS)
    }


    /**
     * Prepare TransactionFeed list from AccountTransactionResponse object
     *
     * @param transactionResponse
     */
    fun prepareTransactionFeed(transactionResponse: AccountTransactionResponse): ArrayList<TransactionFeed> {
        val feedList = ArrayList<TransactionFeed>()
        val transactionList = ArrayList<Transaction>()

        transactionResponse.transactions?.forEach {
            it.isPending = false
            it.date = DateUtils.getCalendarFromDateString(it.effectiveDate.toString())
            transactionList.add(it)
        }

        transactionResponse.pending?.forEach {
            it.isPending = true
            it.date = DateUtils.getCalendarFromDateString(it.effectiveDate.toString())
            transactionList.add(it)
        }

        transactionList.sortByDescending { it.date }

        transactionResponse.account?.let {
            feedList.add(AccountInfoItem(it))
        }

        var dateCache = 0
        transactionList.forEach {
            it.date?.let { date ->
                if (dateCache != date.get(Calendar.DAY_OF_YEAR)) {
                    feedList.add(DateItem(date))
                    dateCache = date.get(Calendar.DAY_OF_YEAR)
                }
            }

            var atmInfo: AtmInfo? = null
            it.atmId?.let { atmId ->
                transactionResponse.atms?.forEach { atm ->
                    if (atmId == atm.id) {
                        atmInfo = atm
                    }
                }
            }

            feedList.add(TransactionItem(it, atmInfo))
        }

        return feedList
    }


}