package com.sgr.test.beemit.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by Shekh Shagar on 06/06/2018
 *
 * Object model for JSON response
 */
@Parcelize
data class AccountTransactionResponse(var account: AccountInfo?,
                                      var transactions: ArrayList<Transaction>?,
                                      var pending: ArrayList<Transaction>?,
                                      var atms: ArrayList<AtmInfo>?) : Parcelable
