package com.sgr.test.beemit.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by Shekh Shagar on 06/06/2018
 */
@Parcelize
data class AccountInfo(var accountName: String?,
                       var accountNumber: String?,
                       var available: Double?,
                       var balance: Double?) : Parcelable
