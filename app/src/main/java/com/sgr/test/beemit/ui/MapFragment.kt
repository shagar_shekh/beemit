package com.sgr.test.beemit.ui

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.sgr.test.beemit.R
import com.sgr.test.beemit.model.AtmInfo

/**
 * Created by Shekh Shagar on 07/06/2018
 */
class MapFragment : SupportMapFragment(), OnMapReadyCallback {
    private lateinit var mMap: GoogleMap

    private var mAtmInfo: AtmInfo? = null

    fun setAtmInfo(atmInfo: AtmInfo?) {
        mAtmInfo = atmInfo
    }

    override fun onMapReady(map: GoogleMap?) {
        mMap = map as GoogleMap
        mAtmInfo?.let {
            val location = LatLng(it.location?.lat!!, it.location?.lng!!)
            val icon = BitmapDescriptorFactory.fromResource(R.drawable.marker_atm_commbank)
            mMap.addMarker(MarkerOptions().position(location).title(it.name).icon(icon))
            mMap.moveCamera(CameraUpdateFactory.newLatLng(location))
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 15F))
            mMap.uiSettings.isZoomControlsEnabled = true
        }
    }

}