package com.sgr.test.beemit.ui

import android.content.Context
import android.os.Build
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.sgr.test.beemit.R
import com.sgr.test.beemit.model.*
import com.sgr.test.beemit.util.DateUtils
import java.util.*

/**
 * Created by Shekh Shagar on 07/06/2018
 *
 * Adapter to show Account info and transactions
 */
class TransactionRecyclerAdapter(context: Context, listItems: ArrayList<TransactionFeed>, itemClickListener: (model: AtmInfo) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        @JvmField
        val LOG_TAG: String = TransactionRecyclerAdapter::class.java.simpleName

        const val VIEW_TYPE_ACCOUNT_INFO = 0
        const val VIEW_TYPE_DATE = 1
        const val VIEW_TYPE_TRANSACTION = 2
    }

    private var mContext = context
    private var mListItems = listItems
    private var mItemClickListener = itemClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View
        when (viewType) {
            VIEW_TYPE_ACCOUNT_INFO -> {
                view = LayoutInflater.from(mContext).inflate(R.layout.adapter_item_account_info_layout, parent, false)
                return AccountInfoViewHolder(view)
            }

            VIEW_TYPE_DATE -> {
                view = LayoutInflater.from(mContext).inflate(R.layout.adapter_item_date_layout, parent, false)
                return DateViewHolder(view)
            }

            VIEW_TYPE_TRANSACTION -> {
                view = LayoutInflater.from(mContext).inflate(R.layout.adapter_item_transaction_layout, parent, false)
                return TransactionViewHolder(view)
            }
            else -> {
                view = LayoutInflater.from(mContext).inflate(R.layout.adapter_item_transaction_layout, parent, false)
                return TransactionViewHolder(view)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (mListItems[position].type) {
            TransactionFeed.TYPE.VIEW_TYPE_ACCOUNT_INFO -> VIEW_TYPE_ACCOUNT_INFO
            TransactionFeed.TYPE.VIEW_TYPE_DATE -> VIEW_TYPE_DATE
            TransactionFeed.TYPE.VIEW_TYPE_TRANSACTION -> VIEW_TYPE_TRANSACTION
            else -> VIEW_TYPE_TRANSACTION
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val adapterPosition = holder.adapterPosition

        when (holder.itemViewType) {
            VIEW_TYPE_ACCOUNT_INFO -> {
                val model = mListItems[adapterPosition] as AccountInfoItem
                val accountInfoHolder = holder as AccountInfoViewHolder
                accountInfoHolder.accountNameTv.text = model.accountInfo.accountName
                accountInfoHolder.accountNumberTv.text = model.accountInfo.accountNumber
                accountInfoHolder.availableFundsTv.text = model.accountInfo.available.toString()
                accountInfoHolder.accountBalanceTv.text = model.accountInfo.balance.toString()
            }
            VIEW_TYPE_DATE -> {
                val model = mListItems[adapterPosition] as DateItem
                val dateViewHolder = holder as DateViewHolder
                dateViewHolder.dateTv.text = DateUtils.getDateStringFromCalenar(model.calendar)
                dateViewHolder.daysCount.text = String.format(mContext.getString(R.string.remaining_days), DateUtils.daysCountFromToday(model.calendar))
            }
            VIEW_TYPE_TRANSACTION -> {
                val model = mListItems[adapterPosition] as TransactionItem
                val transactionViewHolder = holder as TransactionViewHolder

                transactionViewHolder.amountTv.text = model.transaction.amount.toString()
                var description = model.transaction.description
                model.transaction.isPending?.let {
                    if (it) {
                        description = String.format(mContext.getString(R.string.pending), model.transaction.description)
                    }
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    transactionViewHolder.descriptionTv.text = Html.fromHtml(description.toString(), Html.FROM_HTML_MODE_LEGACY)
                } else {
                    transactionViewHolder.descriptionTv.text = Html.fromHtml(description.toString())
                }

                transactionViewHolder.descriptionTv.setCompoundDrawablesWithIntrinsicBounds(0, 0, if (model.atmInfo != null) R.drawable.marker_icon_small else 0, 0)
                transactionViewHolder.descriptionTv.invalidate()
                transactionViewHolder.transactionItemContainer.setOnClickListener {
                    model.atmInfo?.let {
                        mItemClickListener.invoke(it)
                    }
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return mListItems.size
    }

    private inner class AccountInfoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val accountNameTv: TextView = itemView.findViewById(R.id.account_name_tv)
        val accountNumberTv: TextView = itemView.findViewById(R.id.account_number)
        val availableFundsTv: TextView = itemView.findViewById(R.id.available_funds_tv)
        val accountBalanceTv: TextView = itemView.findViewById(R.id.account_balance_tv)
    }

    private inner class DateViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val dateTv: TextView = itemView.findViewById(R.id.date_item_date_tv)
        val daysCount: TextView = itemView.findViewById(R.id.date_item_days_count_tv)
    }

    private inner class TransactionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val transactionItemContainer: View = itemView.findViewById(R.id.transaction_item_container)
        val descriptionTv: TextView = itemView.findViewById(R.id.transaction_item_description_tv)
        val amountTv: TextView = itemView.findViewById(R.id.transaction_item_amount_tv)
    }
}