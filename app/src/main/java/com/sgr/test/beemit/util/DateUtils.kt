package com.sgr.test.beemit.util

import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by Shekh Shagar on 07/06/2018
 *
 * Utility for to handle dates
 */
class DateUtils {
    companion object {
        private const val DATE_FORMAT_API = "dd/MM/yyyy"
        private const val DATE_FORMAT_DISPLAY = "dd MMM yyy"

        /**
         * converts string (dd/MM/yyyy)formatted date to Calendar
         *
         * @param apiDate Date in String format $DATE_FORMAT_API
         * @return Calendar
         */
        fun getCalendarFromDateString(apiDate: String): Calendar {
            return getCalendarFromDateString(apiDate, DATE_FORMAT_API)
        }

        /**
         * converts string formatted date to Calendar
         *
         * @param apiDate Date in String format $DATE_FORMAT_API
         * @param dateFormat input date format
         * @return Calendar
         */
        fun getCalendarFromDateString(apiDate: String, dateFormat: String): Calendar {
            val calendar = Calendar.getInstance()
            try {
                val dateFormatApi = SimpleDateFormat(dateFormat, Locale.getDefault())
                if (!apiDate.isEmpty()) {
                    calendar.time = dateFormatApi.parse(apiDate)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return calendar
        }

        /**
         * Parse calendar object to date string in (07/06/2018) format
         *
         * @param calendar Calendar
         * @return String formatted date
         */
        fun getDateStringFromCalenar(calendar: Calendar): String {
            return getDateStringFromCalenar(calendar, DATE_FORMAT_DISPLAY)
        }

        /**
         * Parse calendar object to date string
         *
         * @param dateFormat date format
         * @param calendar Calendar
         * @return String formatted date
         */
        fun getDateStringFromCalenar(calendar: Calendar, dateFormat: String): String {
            val simpleDateFormat = SimpleDateFormat(dateFormat, Locale.getDefault())
            return simpleDateFormat.format(calendar.time)
        }

        /**
         * Counts day form today's date
         *
         * @param date Calendar to count from today's date
         */
        fun daysCountFromToday(date: Calendar): Long {
            val today = Calendar.getInstance().timeInMillis
            val start = date.timeInMillis
            return TimeUnit.MILLISECONDS.toDays(Math.abs(today - start))
        }
    }
}