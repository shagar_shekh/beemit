# BeemIt

## Tools
* Android Studio 3 with Gradle Plugin 3

####Branches:
* master
* develop

## Dependencies

* Google AppCompat v7
* Google Design Library (design, recycler view, card view)
* Google Play Services (maps)
* Volley
* Google Gson
* JUnit 4
* Espresso